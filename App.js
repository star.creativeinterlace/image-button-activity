import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Text,
  ScrollView
} from 'react-native';

const youTubeIcon = require('./assets/youtube.png');
const houseIcon = require('./assets/house.png');

const App = () => (
  <React.Fragment>
    <View style={styles.homeContainer}>
      <TouchableOpacity onPress={() => alert("Home pressed")} style={{ width: '50%', marginTop: 100 }}>
        <ImageBackground source={houseIcon} style={{ height: '90%', width: '100%'}} />
      </TouchableOpacity>
    </View>
    <View style={styles.youtubeContainer}>
      <TouchableOpacity onPress={() => alert("Youtube pressed")} style={{ width: '50%',  height: '100%' }}>
        <ImageBackground source={youTubeIcon} style={{ height: '120%' }} />
      </TouchableOpacity>
    </View>
    <View style={styles.buttonContainer}>
      <TouchableOpacity onPress={() => alert("Button pressed")} 
        style={styles.button}>
        <Text style={{ color: 'white', fontWeight: '500' }}>YouTube Button</Text>
      </TouchableOpacity>
    </View>
  </React.Fragment>
);

const styles = StyleSheet.create({
  homeContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    maxHeight: '40%'
  },
  youtubeContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    maxHeight: '15%'
  },
  buttonContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    maxHeight: '30%'
  },
  button: {
    width: '50%',
    height: '20%',
    backgroundColor: 'gray',
    borderRadius: 30,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center'
  }
});

export default App;